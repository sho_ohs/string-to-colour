#!/usr/bin/python
import random
import sys

def hexit(inputstr):
    random.seed(inputstr)
    number = (random.randint(0,16777215))
    return number

def errorout():
    print("Usage: `str [-q] \"string\"`")
    quit()
    

instr = ""
try:
    arg1 = sys.argv[1]
except:
    errorout()

if arg1 == "-q":
    try:
        instr = sys.argv[2]
    except:
        errorout()
    
    print("#%06X" % hexit(instr))

else:
    instr = arg1
    print("Input string: " + instr)
    print("Colour hex output: #%06X " % hexit(instr))
